from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todolist = TodoList.objects.all()
    todo_list_detail = TodoItem.objects.all()

    context = {
        "todo_list_detail": todo_list_detail,
        "todolist": todolist,
    }
    return render(request, "todolist.html", context)


def todo_list_detail(request, id):
    todo_list_detail = TodoItem.objects.all()
    todolist_detail = TodoList.objects.get(id=id)


    context = {

        "todo_list_detail": todo_list_detail,
        "todolist_detail": todolist_detail
    }
    return render(request, "todo_list_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail",id = todolist.id )
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "create.html", context)


def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolists = form.save()
            return redirect("todo_list_detail",id=todolists.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todolist": todolist,
        "form": form,
    }
    return render(request, "edit.html", context)


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "delete.html")



def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm()

    context = {


        "form":form,
    }


    return render(request,"todo_item_create.html" ,context)



def todo_item_update(request, id):
    todoitems = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitems)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoItemForm(instance=todoitems)

    context = {
        "form":form,
    }

    return render(request, "todo_item_update.html", context)
